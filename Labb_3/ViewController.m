//
//  ViewController.m
//  Labb_3
//
//  Created by Stjernström on 2015-02-02.
//  Copyright (c) 2015 Stjernström. All rights reserved.
//

#import "ViewController.h"
#import "Task.h"
#import "AddTask.h"

@interface ViewController ()

//@property (nonatomic) NSMutableArray *getList;
@property (nonatomic) Task *task;
@property (nonatomic) AddTask *add;

@end

@implementation ViewController

- (void)AddNewEntry: (NSString*)newEntry {
    [self.getList addObject:newEntry];
}

- (NSMutableArray*)getList
{
    if (!_getList) {
        _getList = [@[@"Go create a new task", @"Remove this task", @"Try something else"] mutableCopy];
    }
    return _getList;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getList];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString: @"AddSegue"])
    {
        self.add = [segue destinationViewController];
        self.add.list = self.getList;
    }
    
    else if ([segue.identifier isEqualToString: @"TaskSegue"])
    {
        self.task = [segue destinationViewController];
        self.task.list = self.getList;
    }
}

@end
