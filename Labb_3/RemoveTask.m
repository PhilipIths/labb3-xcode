//
//  RemoveTask.m
//  Labb_3
//
//  Created by Stjernström on 2015-02-05.
//  Copyright (c) 2015 Stjernström. All rights reserved.
//

#import "RemoveTask.h"
#import "Task.h"

@interface RemoveTask ()

@property (nonatomic) Task *task;
@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@property (weak, nonatomic) IBOutlet UIButton *button;

@end

@implementation RemoveTask

- (IBAction)RemoveButton:(id)sender {
    [self DeleteEntry:self.index];
}

- (void)DeleteEntry: (NSInteger)entry {
    [self.list removeObjectAtIndex:entry];
    self.button.enabled = NO;
    self.textLabel.text = @"Task removed";
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    self.task = [segue destinationViewController];
    self.task.list = self.list;
}


@end
