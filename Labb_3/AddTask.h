//
//  AddTask.h
//  Labb_3
//
//  Created by Stjernström on 2015-02-05.
//  Copyright (c) 2015 Stjernström. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddTask : UIViewController

@property (nonatomic) NSMutableArray *list;

@end
