//
//  AddTask.m
//  Labb_3
//
//  Created by Stjernström on 2015-02-05.
//  Copyright (c) 2015 Stjernström. All rights reserved.
//

#import "AddTask.h"
#import "ViewController.h"

@interface AddTask ()

//@property (nonatomic) NSMutableArray *list;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (nonatomic) ViewController *view;

@end

@implementation AddTask

- (IBAction)AddTaskButton:(id)sender {
    if ([self.textField.text length] == 0) {
        return;
    }
    
    [self.list addObject: self.textField.text];
    self.textField.text = @"";
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    self.view = [segue destinationViewController];
    self.view.getList = self.list;
}

@end
