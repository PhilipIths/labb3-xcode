//
//  Task.h
//  Labb_3
//
//  Created by Stjernström on 2015-02-02.
//  Copyright (c) 2015 Stjernström. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Task : UITableViewController

@property (nonatomic) NSMutableArray *list;

@end
